#! /usr/bin/env node

/*
 * pushbullet-cli
 * 
 *
 * Copyright (c) 2015 Mathias Claes
 * Licensed under the MIT license.
 */

'use strict';

var fs = require('fs');
var path = require('path');
var osenv = require('osenv');
var PushBullet = require('pushbullet');


function processArguments(pusher, arg) {
  
  var fileArg;
  
  if ( arg.length != 1 )
  {
    console.log("no input file provided");
    return;
  }
  
  fileArg = arg[0];
  
  if (!(fs.existsSync(fileArg))) {
    console.log("File does not exist!");
    return;
  }
  
  var options = {
    limit: 10
  };

  // get the devices
  pusher.devices(options, function(error, response) {
    
    if ( error) {
      console.log(error);
      return;
    }
    
    // filter the devices from the response
    var devices = response.devices;
    
    // reset index
    var idx = 0;
    
    for( ; idx < devices.length; idx++ )
    {
      // check if type "chrome" exist
      if( devices[idx].type === "chrome" )
      {
        break;
      }
    }
    
    // if nothing found, return
    if( idx > devices.length ) {
      console.log("No Chrome device found");
      return;
    }
    
    // push the file to the device
    pusher.file(devices[idx].iden, fileArg, path.basename(fileArg), function(error, response) {
      
      // log error if any
      if(error) {
        console.log(error);
      }
      // lof response if any
      if(response) {
        console.log("Successfully pushed file, Url: " + response.file_url);
      }
    });

  });
  
}

var main = function() 
{
  
  var home = osenv.home();
  var apiKey;
  
  // Try to load the PushBullet API key
  try {
    apiKey = require(path.join(home, '.pushbullet.json')).api_key;
  } catch (err) {
    console.log('Failed to load PushBullet API Key. Create ~/.pushbullet.json with the api_key parameter set.');
  }
  
  // Create new pushbullet object
  var pusher = new PushBullet(apiKey);

  // Get arguments from cli
  var arg = process.argv.slice(2);
  
  processArguments(pusher, arg);
  
}


if (require.main === module) {
    main();
}

