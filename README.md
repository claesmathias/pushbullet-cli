# pushbullet-cli

Command line tool to push files to pushbullet.

## Getting Started
Install the module with: `npm install pushbullet-cli`

```javascript
var pushbullet_cli = require('pushbullet-cli');
pushbullet_cli.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2015 Mathias Claes  
Licensed under the MIT license.
